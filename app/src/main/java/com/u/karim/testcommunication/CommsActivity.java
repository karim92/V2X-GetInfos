package com.u.karim.testcommunication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Surface;
import android.widget.Toast;

public class CommsActivity extends AppCompatActivity implements LocationListener, SensorEventListener {

    private static final int PERMS_CALL_ID = 1234;
    private LocationManager lm;
    private SensorManager sm;
    private Sensor sensor;
    private float x, y, z;

    private float lastX, lastY, lastZ, lastLat, lastLong;
    private float deltaX = 0;
    private float deltaY = 0;
    private float deltaZ = 0;
    private float vibrateThreshold = 0;
    public Vibrator v;
    int READINGRATE = 5000000;
    double R = 6371; // km

    double latitude;
    double longitude ;
    private Display dp;
    private boolean begin=false;
    private  static String address = null;

    public BluetoothAdapter BTAdapter = BluetoothAdapter.getDefaultAdapter();
    private static final String TAG = "CommsActivity";
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    //final String payload=null;

    public class ConnectThread extends Thread {
        private ConnectThread(BluetoothDevice device) throws IOException {

            BluetoothSocket tmp = null;
            mmDevice = device;
            try {
                UUID uuid = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee");
                tmp = mmDevice.createRfcommSocketToServiceRecord(uuid);
            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            mmSocket = tmp;
            BTAdapter.cancelDiscovery();
            try {
                mmSocket.connect();
            } catch (IOException connectException) {
                Log.v(TAG, "Connection exception!");
                try {
                    mmSocket.close();

                } catch (IOException closeException) {

                }
            }

           send();

        }


        public void send() throws IOException {
            String msg = String.valueOf(latitude)+":"+String.valueOf(longitude)+":"+String.valueOf(x)+":"+String.valueOf(y)+":"+String.valueOf(z);
            Log.i("DEBUG2", String.valueOf(latitude)+":"+String.valueOf(longitude)+":"+String.valueOf(x)+":"+String.valueOf(y)+":"+String.valueOf(z));
            OutputStream mmOutputStream = mmSocket.getOutputStream();
            mmOutputStream.write(msg.getBytes());
            //receive();

            try {

                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Problems occurred!");
                return;
            }
        }
/*
        public void receive() throws IOException {
            InputStream mmInputStream = mmSocket.getInputStream();
            byte[] buffer = new byte[256];
            int bytes;

            try {
                bytes = mmInputStream.read(buffer);
                String readMessage = new String(buffer, 0, bytes);
                Log.d(TAG, "Received: " + readMessage);
                TextView voltageLevel = (TextView) findViewById(R.id.Voltage);
                voltageLevel.setText("Voltage level\n" +"DC " + readMessage + " V");
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Problems occurred!");
                return;
            }
        }*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comms);

        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        dp = getWindowManager().getDefaultDisplay();
        //initialize vibration
        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        final Intent intent = getIntent();
        address = intent.getStringExtra(MainActivity.EXTRA_ADDRESS);
       Button voltButton = (Button) findViewById(R.id.meas);

       voltButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                begin = true;
            }
        });

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, 0);
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        CheckPermissions();
        sm.registerListener(this, sensor, READINGRATE);
        vibrateThreshold = 2;
    }

    private void beginSend(String address){
        final BluetoothDevice device = BTAdapter.getRemoteDevice(address);
        try {

            new ConnectThread(device).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void CheckPermissions() {
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            }, PERMS_CALL_ID);
            return;
        }
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
        }
        if (lm.isProviderEnabled(LocationManager.PASSIVE_PROVIDER)) {
            lm.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 0, this);
        }
        if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMS_CALL_ID ){
            CheckPermissions();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (lm !=null) {
            lm.removeUpdates(this);
        }
        sm.unregisterListener(this, sensor);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // En fonction de l'orientation de l'appareil, on corrige les valeurs x et y du capteur
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            switch (dp.getRotation()) {
                case Surface.ROTATION_0:
                    x = event.values[0];
                    y = event.values[1];
                    break;
                case Surface.ROTATION_90:
                    x = -event.values[1];
                    y = event.values[0];
                    break;
                case Surface.ROTATION_180:
                    x = -event.values[0];
                    y = -event.values[1];
                    break;
                case Surface.ROTATION_270:
                    x = event.values[1];
                    y = -event.values[0];
                    break;
            }
        }
        z = event.values[2];
        updateAccelero(x, y, z);
        deltaX = Math.abs(lastX - x);
        deltaY = Math.abs(lastY - y);
        deltaZ = Math.abs(lastZ - z);
        // if the change is below 2, it is just plain noise
        if (deltaX < 2)
            deltaX = 0;
        if (deltaY < 2)
            deltaY = 0;
        // set the last know values of x,y,z
        lastX = x;
        lastY = y;
        lastZ = z;
        if ((deltaZ  > vibrateThreshold) || (deltaY > vibrateThreshold) || (deltaZ > vibrateThreshold)) {
          v.vibrate(50);
        } else {

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
    private void updatePosition(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        final TextView pos = (TextView) findViewById(R.id.Position);
        pos.setText(String.valueOf(latitude)+" , "+String.valueOf(longitude));
        if (begin) {
            beginSend(address);
        }
        /*
        double dLat = Math.toRadians(latitude-lastLat);
        double dLon = Math.toRadians(longitude-lastLong);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(latitude)) * Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = R * c;

        if ((deltaZ  <= vibrateThreshold) || (deltaY <= vibrateThreshold) || (deltaZ <= vibrateThreshold)) {
            if (distance <= 5) {
                beginSend(address);
            }
        }
       */
    }
    private  void updateAccelero(float x, float y, float z) {
        final TextView Accelero = (TextView) findViewById(R.id.Accelero);
        Accelero.setText(String.valueOf(x)+" , "+String.valueOf(y)+" , "+String.valueOf(z));
    }
    @Override
    public void onLocationChanged(Location location) {
        updatePosition(location);
    }


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    @Override
    protected void onStop() {
        super.onStop();
        try {
            mmSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}